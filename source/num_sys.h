#pragma once
#ifndef NS_H
#define NS_H
#include <ostream>
#include <vector>
#include <initializer_list>
#include <math.h>


template<int sin, int sout>
class num_sys {
public:
	num_sys()=default;
	num_sys(num_sys&);
	num_sys(num_sys&&);
	void clear();
	void setValue(std::initializer_list<char>);
	~num_sys()=default;
	num_sys operator+(num_sys&);
	num_sys operator-(num_sys&);
private:
	std::vector<char> num_vec;
	void add(int, int);
	void remove(int, int);
	template<int sin1,int sout1>
friend std::ostream& operator << (std::ostream&, num_sys<sin1, sout1>&);
};
int getValue(char);
char getValue(int);


char alphabet[35] = {
'1','2','3','4','5','6','7','8','9',
'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
//GET_VALUE
 char getValue(int val){
 if(val==0)return '0';
 return alphabet[val-1];
 }
 int getValue(char val){
 int sm=0, bg=60, id=30;
 while(val!=alphabet[id]){
    if(val<alphabet[id])
        {
            bg = id;
            id = (sm+bg)/2;
        }else{
           sm = id;
           id = (sm+bg)/2;
    }
        }
 return id+1;
}
template<int sin, int sout>
num_sys<sin, sout>::num_sys(num_sys<sin, sout>&toneedcopy){
	std::size_t i=0;
	for(char cc:toneedcopy.num_vec){
		if(num_vec.size()==i)
		num_vec.push_back(cc);
		else
		num_vec[i]=toneedcopy.num_vec[i];
	i++;
	}
}
template<int sin,int sout>
num_sys<sin,sout>::num_sys(num_sys<sin,sout>&&toneedmove){
	std::cout<<"Here Error";
}
//ADD
template<int sin, int sout>
void num_sys<sin, sout>::add(int i, int num){
 if(i==num_vec.size())num_vec.push_back('0');
	if(
                ((num_vec[i]=='0'?0:getValue(num_vec[i]))+num)
		>=sout
		 ){

 add(i+1, ((num_vec[i]=='0'?0:getValue(num_vec[i]))+num)/sout);
  int n=1;
 while
          (
           (num_vec[i]=='0'?0:getValue(num_vec[i]))+num-sout*n
           >=0
           )n++;
num_vec[i]=getValue
	  ((num+(num_vec[i]=='0'?0:getValue(num_vec[i])))-(--n*sout)
	 );
 }else{
num_vec[i]=getValue((num+(num_vec[i]=='0'?0:getValue(num_vec[i]))));
 return;
 }               
}
//REMOVE
template<int sin, int sout>
void num_sys<sin, sout>::remove(int i, int num){
 if(i==num_vec.size()){return;}
        if(
                ((num_vec[i]=='0'?0:getValue(num_vec[i]))-num)
                <0
                 ){
 remove(i+1,1);
num_vec[i]=sout+((num_vec[i]=='0'?0:getValue(num_vec[i]))-num)==0?'0':getValue(sout+((num_vec[i]=='0'?0:getValue(num_vec[i]))-num));
 }else{
num_vec[i]=getValue(num_vec[i])-num==0?'0':getValue(getValue(num_vec[i])-num);
 return;
 }
}
//OPERATOR_<<
template<int sin, int sout>
std::ostream& operator << (std::ostream& out, num_sys<sin, sout>& num){
for(int i=num.num_vec.size()-1; i>=0; i--)
 out<<num.num_vec[i];
return out;
}
//SET_VALUE
template<int sin, int sout>
void num_sys<sin, sout>::setValue(std::initializer_list<char> N) {
std::size_t i;
num_vec.push_back('0');
   i=0;
	for(char cc:N){
if(cc=='0'){i++; continue;} 
else{ this->add(0,getValue(cc)*pow(sin,N.size()-1-i));i++;}
	}
	}
//CLEAR
template<int sin, int sout>
   void num_sys<sin, sout>::clear(){this->num_vec.clear();}
//OPERATOR_+
   template<int sin, int sout>
   num_sys<sin,sout> num_sys<sin,sout>::operator+(num_sys<sin, sout>& right){
	   num_sys<sin, sout>cop(*(this));
	   int i=0;
	   for(char cc:right.num_vec){
		   if(cc=='0'){i++; continue;}
		   else
	cop.add(0,getValue(cc)*pow(sout,i++));
}
	   return cop;
   }
//OPERATOR_-
template<int sin, int sout>
num_sys<sin,sout> num_sys<sin,sout>::operator -(num_sys<sin,sout>&right){
 num_sys<sin, sout>cop(*(this));
           int i=0;
           for(char cc:right.num_vec){
                   if(cc=='0'){i++; continue;
		   }
                   else{
        cop.remove(0,(getValue(cc)*pow(sout,i++)));
		   }
		   }
           return cop;
}
#endif // !NS_H
